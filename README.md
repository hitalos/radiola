# Radiola

This project is a webserver that simulates a jukebox.

## Dependencies

* NodeJS
* One of these players:
  * `mplayer`
  * `afplay`
  * `mpg123`
  * `mpg321`
  * `play`
  * `omxplayer`
  * `aplay`
  * `cmdmp3`

## Install

Clone the project and run:

    npm install

## Running

    npm start

## Using

1. Access `http://<IP>:3000` and go to **"admin"** tab.
2. Add a folders to be scanned.
3. On another tab, add songs to **"playlist"**.
4. Use tab **"player"** to manipulate the *playlist*.
5. The songs are played by **"webserver process"**. You can close the browser.
