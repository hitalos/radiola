const bodyParser = require('body-parser')
const compression = require('compression')
const express = require('express')
const path = require('path')
const routes = require('./routes')
require('./listeners')

process.on('uncaughtException', console.error)

const app = express()

app.set('view engine', 'pug')
app.set('views', './src/resources/views')
app.locals.pretty = true

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json({ limit: '1mb' }))
app.use(compression())
app.use(express.static(path.resolve(__dirname, '../public')))
app.use(routes)

// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error('Not Found')
  err.status = 404
  next(err)
})

app.use((err, req, res, next) => {
  res.status(err.status || 500)
  if (app.get('env') === 'development') {
    res.render('error', { error: err })
  } else {
    res.render('error', { error: { message: err.msg } })
  }
  next()
})

module.exports = app
