const Sequelize = require('sequelize')

const db = new Sequelize(
  'sqlite://Radiola.db',
  {
    operatorsAliases: false,
    logging: process.env.NODE_ENV === 'production' ? false : console.log
  }
)

const Artist = db.import('./models/artist')
const Album = db.import('./models/album')
const Song = db.import('./models/song')
db.import('./models/folder')

Artist.hasMany(Album)
Album.hasMany(Song)
Album.belongsTo(Artist)
Song.belongsTo(Album)

db.sync()

module.exports = db
