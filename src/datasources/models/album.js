module.exports = (conn, DataTypes) =>
  conn.define('album', {
    title: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: 'album_by_artist',
    },
    year: {
      type: DataTypes.INTEGER,
      set(val) {
        this.setDataValue('year', val.toString().substring(0, 4))
      },
    },
    cover: {
      type: DataTypes.BLOB,
    },
    artist_id: {
      type: DataTypes.INTEGER,
      unique: 'album_by_artist',
    },
  }, {
    underscored: true,
    timestamps: false,
    onDelete: 'cascade',
  })
