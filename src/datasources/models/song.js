module.exports = (conn, DataTypes) =>
  conn.define('song', {
    title: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    track: {
      type: DataTypes.INTEGER,
    },
    disk: {
      type: DataTypes.INTEGER,
    },
    duration: {
      type: DataTypes.INTEGER,
    },
    played: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    path: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
  }, {
    underscored: true,
    timestamps: false,
  })
