module.exports = (conn, DataTypes) =>
  conn.define('folder', {
    path: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
  }, {
    underscored: true,
  })
