module.exports = (conn, DataTypes) =>
  conn.define('artist', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
  }, {
    underscored: true,
    timestamps: false,
    onDelete: 'cascade',
  })
