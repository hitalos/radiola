const router = require('express').Router()
const db = require('../../datasources')

const Artist = db.models.artist
const Album = db.models.album
const Song = db.models.song

router.get('/', (req, res) => {
  const order = [req.query.order || ['title']]
  const where = {}
  if (req.query.artist) {
    where.name = req.query.artist
    order.unshift('year')
  }
  Album.findAll({
    attributes: [
      'id',
      'title',
      'year',
      'cover',
      [db.fn('COUNT', db.col('Songs.id')), 'songs']
    ],
    order,
    include: [
      { model: Artist, attributes: ['id', 'name'], where },
      { model: Song, attributes: [] },
    ],
    group: ['album.id'],
    raw: true
  }).then((result) => {
    const albums = result.map(album => ({
      id: album.id,
      title: album.title,
      year: album.year,
      artist: album.artist,
      songs: album.songs,
      cover: album.cover ? album.cover.toString('base64') : null,
    }))
    res.json(albums)
  }).catch(() => {
    res.json([])
  })
})
router.put('/', (req, res) => {
  const album = req.body
  delete album.songs
  if (album.cover) {
    album.cover = Buffer.from(album.cover.replace(/^data:image\/jpeg;base64,/, ''), 'base64')
  } else if (album.cover === '') {
    album.cover = null
  } else {
    delete album.cover
  }
  Album.findById(album.id)
    .then((result) => {
      if (result) {
        result.update(album)
          .then((updated) => {
            if (updated.cover) {
              const cover = updated.cover.toString('base64')
              res.json({ ...updated.toJSON(), cover })
              return
            }
            res.json(updated)
          })
      } else {
        res.status(404).json([])
      }
    })
})

router.delete('/', (req, res) => {
  Album.destroy({ where: req.body }).then((result) => {
    res.json(result)
  }).catch((err) => {
    res.status(500).json(err)
  })
})

module.exports = router
