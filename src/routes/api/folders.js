const debug = require('debug')('Radiola:routes:api:folders')
const fs = require('fs')
const path = require('path')
const router = require('express').Router()
const db = require('../../datasources')

const Folder = db.models.folder
const home = process.env.HOME

router.get('/', (req, res) => {
  const order = [req.query.order || ['path']]
  Folder.findAll({ order })
    .then((result) => {
      const folders = result.map(folder => ({
        id: folder.id,
        path: folder.path,
      }))
      res.json(folders)
    }).catch(() => {
      res.json([])
    })
})
router.post('/', (req, res) => {
  const folderPath = path.resolve(req.body.folder.replace(/^~/, home))
  fs.stat(folderPath, (err, stats) => {
    if (err) {
      res.status(409).json({ errors: [{ message: err.message }] })
      return
    }
    if (stats.isDirectory()) {
      Folder.create({ path: folderPath })
        .then((folder) => {
          process.emit('new folder', folder.path)
          res.status(201).json(folder)
        })
        .catch(result => res.status(409).json({ errors: result.errors }))
    } else {
      res.status(409).json({ errors: [{ message: 'Path is not a folder' }] })
    }
  })
})
router.delete('/', (req, res) => {
  Folder.destroy({ where: req.body })
    .then(() => {
      debug(`Removing musics on "${req.body.path}" from database`)
      process.emit('remove songs on dir', req.body.path)
      res.status(202).json({ message: 'OK' })
    })
    .catch(err => console.log(err))
})

module.exports = router
