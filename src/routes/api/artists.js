const router = require('express').Router()
const db = require('../../datasources')

const Artist = db.models.artist
const Album = db.models.album
const Song = db.models.song

router.get('/', (req, res) => {
  const order = [req.query.order || ['name']]
  Artist.findAll({ order })
    .then(result => Promise.all(result.map(async (artist) => {
      const albums = await Album.findAll({
        attributes: [[db.fn('COUNT', db.col('Songs.id')), 'songsCount']],
        where: { artist_id: artist.id },
        include: { model: Song, attributes: [] },
        group: ['album.id']
      })
      const songsCount = albums.reduce((sum, album) => sum + album.toJSON().songsCount, 0)
      return Promise.resolve(Object.assign({ albumsCount: albums.length, songsCount }, artist.toJSON()))
    })))
    .then(result =>
      res.json(result))
    .catch((e) => {
      res.json(e)
    })
})

router.delete('/', (req, res) => {
  Artist.destroy({ where: req.body }).then((result) => {
    res.json(result)
  }).catch((err) => {
    res.status(500).json(err)
  })
})

module.exports = router
