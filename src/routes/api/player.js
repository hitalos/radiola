const router = require('express').Router()

router.get('/', (req, res) => {
  process.emit('getList', (list) => {
    res.json({ playlist: list })
  })
})

router.post('/add', (req, res) => {
  process.emit('add to playlist', req.body)
  res.json({ message: 'ok' })
})

router.get('/play', (req, res) => {
  process.emit('play list')
  res.json({ message: 'ok' })
})

router.get('/stop', (req, res) => {
  process.emit('stop')
  res.json({ message: 'ok' })
})

router.get('/pause', (req, res) => {
  process.emit('pause')
  res.json({ message: 'ok' })
})

router.get('/resume', (req, res) => {
  process.emit('resume')
  res.json({ message: 'ok' })
})

router.get('/next', (req, res) => {
  process.emit('play next')
  res.json({ message: 'ok' })
})

module.exports = router
