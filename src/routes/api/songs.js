const router = require('express').Router()
const db = require('../../datasources')

const Song = db.models.song
const Album = db.models.album

router.get('/', (req, res) => {
  const order = [req.query.order || ['title']]
  const where = {}
  if (req.query.album) {
    where.title = req.query.album
    order.unshift('track')
  }
  Song.findAll({
    order,
    include: [
      { model: Album, attributes: ['id', 'title'], where }
    ]
  }).then((result) => {
    const songs = result.map((song) => {
      const minutes = Math.round(song.duration / 60).toString().padStart(2, '0')
      const seconds = Math.round(song.duration % 60).toString().padStart(2, '0')
      return {
        id: song.id,
        title: song.title,
        duration: `${minutes}:${seconds}`,
        played: song.played,
        path: song.path,
      }
    })
    res.json(songs)
  }).catch(() => {
    res.json([])
  })
})

module.exports = router
