const router = require('express').Router()
const albums = require('./albums')
const artists = require('./artists')
const folders = require('./folders')
const player = require('./player')
const songs = require('./songs')

router.get('/', (req, res) => {
  res.json({ apiVersion: '1.0' })
})

router.use('/albums', albums)
router.use('/artists', artists)
router.use('/folders', folders)
router.use('/player', player)
router.use('/songs', songs)

module.exports = router
