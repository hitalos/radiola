/* eslint no-use-before-define: 0 */
const {
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLNonNull,
  GraphQLString,
  GraphQLInt,
  GraphQLList
} = require('graphql')

const { models, Sequelize } = require('../../datasources')

const { Op } = Sequelize
const {
  artist,
  album,
  song,
  folder
} = models

const Artist = new GraphQLObjectType({
  description: 'Band, singer or author.',
  name: 'Artist',
  fields: () => ({
    id: { type: GraphQLInt },
    name: { type: GraphQLString },
    albums: {
      type: new GraphQLList(Album),
      resolve: parent => parent.getAlbums()
    },
    albumsCount: {
      type: GraphQLInt,
      resolve: parent => parent.getAlbums().then(albums => albums.length)
    }
  })
})

const Album = new GraphQLObjectType({
  description: 'Collections of songs released normally in Compact Disc format.',
  name: 'Album',
  fields: () => ({
    id: { type: GraphQLInt },
    title: { type: GraphQLString },
    year: { type: GraphQLInt },
    cover: {
      type: GraphQLString,
      resolve: ({ cover }) => (cover ? cover.toString('base64') : null)
    },
    artist: {
      type: Artist,
      resolve: parent => parent.getArtist()
    },
    songs: {
      type: new GraphQLList(Song),
      resolve: parent => parent.getSongs()
    },
    songsCount: {
      type: GraphQLInt,
      resolve: parent => parent.getSongs().then(songs => songs.length)
    }
  }),
})

const Song = new GraphQLObjectType({
  description: 'Metadata from mp3 files found in scanned folders',
  name: 'Song',
  fields: () => ({
    id: { type: GraphQLInt },
    title: { type: GraphQLString },
    track: { type: GraphQLInt },
    disk: { type: GraphQLInt },
    duration: { type: GraphQLInt },
    played: { type: GraphQLInt },
    path: { type: GraphQLString },
    album: {
      type: Album,
      resolve: parent => parent.getAlbum()
    },
  })
})

const Folder = new GraphQLObjectType({
  description: 'Path on local file system to be scanned',
  name: 'Folder',
  fields: () => ({
    id: { type: GraphQLInt },
    path: { type: GraphQLString },
  })
})

module.exports = new GraphQLSchema({
  query: new GraphQLObjectType({
    description: 'Read-only operations',
    name: 'RootQuery',
    fields: {
      songs: {
        description: 'List Songs',
        type: new GraphQLList(Song),
        args: {
          title: { type: GraphQLString },
        },
        resolve: (parent, { title }) => {
          const where = {}
          if (title) where.title = { [Op.like]: `%${title}%` }
          return song.findAll({ where })
        }
      },
      albums: {
        description: 'List Albums',
        type: new GraphQLList(Album),
        args: {
          title: { type: GraphQLString },
          year: { type: GraphQLInt },
        },
        resolve: (parent, { title, year }) => {
          const where = {}
          if (title) where.title = { [Op.like]: `%${title}%` }
          if (year) where.year = year
          return album.findAll({ where })
        }
      },
      artists: {
        description: 'List Artists',
        type: new GraphQLList(Artist),
        args: {
          name: { type: GraphQLString },
        },
        resolve: (parent, { name }) => {
          const where = {}
          if (name) where.name = { [Op.like]: `%${name}%` }
          return artist.findAll({ where })
        }
      },
      folders: {
        description: 'List Folders',
        type: new GraphQLList(Folder),
        args: {
          path: { type: GraphQLString },
        },
        resolve: (parent, { path }) => {
          const where = {}
          if (path) where.path = { [Op.like]: `%${path}%` }
          return folder.findAll({ where })
        }
      },
      song: {
        description: 'Find song by id',
        type: Song,
        args: {
          id: { type: new GraphQLNonNull(GraphQLInt) }
        },
        resolve: (parent, args) => song.findOne({ where: args })
      },
      album: {
        description: 'Find album by id',
        type: Album,
        args: {
          id: { type: new GraphQLNonNull(GraphQLInt) }
        },
        resolve: (parent, args) => album.findOne({ where: args })
      },
      artist: {
        description: 'Find artist by id',
        type: Artist,
        args: {
          id: { type: new GraphQLNonNull(GraphQLInt) }
        },
        resolve: (parent, args) => artist.findOne({ where: args })
      },
      folder: {
        description: 'Find folder by id',
        type: Folder,
        args: {
          id: { type: new GraphQLNonNull(GraphQLInt) }
        },
        resolve: (parent, args) => folder.findOne({ where: args })
      },
    }
  })
})
