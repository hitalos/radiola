const api = require('./api')
const graphql = require('./graphql')

const router = require('express').Router()

router.get('/', (req, res) => {
  res.render('index')
})

router.use('/api', api)
router.use('/graphql', graphql)

router.get(/^\/(admin|artists|albums|songs|player)/, (req, res) => {
  res.render('index')
})

module.exports = router
