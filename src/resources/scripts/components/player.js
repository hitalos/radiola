/* eslint no-unused-vars: 0, no-undef: 0 */
const Player = {
  template: '#player-template',
  data() {
    return {
      playlist: [],
      paused: true,
    }
  },
  created() {
    axios('/api/player').then((result) => {
      this.playlist = result.data.playlist
    })
  },
  methods: {
    play() {
      axios.get('/api/player/play').then(() => {
        this.paused = false
      })
    },
    pause() {
      if (this.paused) {
        axios.get('/api/player/resume').then(() => {
          this.paused = false
        })
      } else {
        axios.get('/api/player/pause').then(() => {
          this.paused = true
        })
      }
    },
    stop() {
      axios.get('/api/player/stop').then(() => {
        this.paused = false
      })
    },
    next() {
      axios.get('/api/player/next')
    },
  },
}
