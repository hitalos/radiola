/* eslint no-unused-vars: 0, no-undef: 0 */
const Album = {
  template: '#albums-template',
  data() {
    return {
      albums: [],
    }
  },
  created() {
    const query = this.$route.params.name ? `?artist=${this.$route.params.name}` : ''
    axios(`/api/albums${query}`).then((result) => {
      this.albums = result.data
    })
  },
}
