/* eslint no-unused-vars: 0, no-undef: 0 */
const Song = {
  template: '#songs-template',
  data() {
    return {
      songs: [],
    }
  },
  created() {
    const query = this.$route.params.name ? `?album=${this.$route.params.name}` : ''
    axios(`/api/songs${query}`).then((result) => {
      this.songs = result.data
    })
  },
  methods: {
    add(song) {
      axios({
        method: 'post',
        url: '/api/player/add',
        headers: {
          'Content-Type': 'application/json',
        },
        data: song,
      })
    }
  }
}
