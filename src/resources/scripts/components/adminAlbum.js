/* eslint no-unused-vars: 0, no-undef: 0 */
const adminAlbum = {
  template: '#admin-album-template',
  data() {
    return {
      title: this.album.title,
      year: this.album.year,
      cover: this.album.cover,
      file: null,
    }
  },
  props: ['album'],
  methods: {
    changeCover(event) {
      const files = event.target.files || event.dataTransfer.files
      if (!files.length) return
      this.createImage(files[0])
    },
    createImage(file) {
      const image = new Image()
      const reader = new FileReader()

      reader.onload = (e) => {
        this.cover = e.target.result.replace(/^data:image\/jpeg;base64,/, '')
      }
      reader.readAsDataURL(file)
    },
    removeCover() {
      this.cover = ''
    },
    update(album) {
      axios({
        method: 'put',
        url: '/api/albums',
        headers: {
          'Content-Type': 'application/json',
        },
        data: {
          id: this.album.id,
          title: this.title,
          year: this.year,
          cover: this.cover,
        },
      }).then((result) => {
        this.title = result.data.title
        this.year = result.data.year
        this.cover = result.data.cover
      })
    }
  }
}
