/* eslint no-unused-vars: 0, no-undef: 0 */
const adminFolders = {
  template: '#admin-folder-template',
  data() {
    return {
      folders: [],
      errors: [],
    }
  },
  created() {
    axios('/api/folders').then((result) => {
      this.folders = result.data
    })
  },
  methods: {
    postFolder(e) {
      if (e.target.value.trim() !== '') {
        axios({
          method: 'post',
          url: '/api/folders',
          headers: {
            'Content-Type': 'application/json',
          },
          data: {
            folder: e.target.value,
          },
        }).then((result) => {
          this.folders.push(result.data)
          e.target.value = ''
        }).catch((result) => {
          this.errors = result.response.data.errors
        })
      }
    },
    deleteFolder(folder) {
      axios({
        method: 'delete',
        url: '/api/folders',
        headers: {
          'Content-Type': 'application/json',
        },
        data: folder,
      }).then((result) => {
        if (result.status === 202) {
          this.folders.forEach((item, index) => {
            if (item.id === folder.id) this.folders.splice(index, 1)
          })
        }
      }).catch((result) => {
        this.errors = result.response.data.errors
      })
    },
    closeErrors() {
      this.errors = []
    }
  }
}
