/* eslint no-undef: 0 */
Vue.component('hHeader', {
  template: '#header-template',
  data() {
    return {
      showMenu: false
    }
  },
  methods: {
    toogleMenu() {
      this.showMenu = !this.showMenu
    },
  },
})
