/* eslint no-unused-vars: 0, no-undef: 0 */
const Artist = {
  template: '#artists-template',
  data() {
    return {
      artists: [],
    }
  },
  created() {
    axios('/api/artists').then((result) => {
      this.artists = result.data
    })
  },
}
