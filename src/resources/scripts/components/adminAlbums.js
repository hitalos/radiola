/* eslint no-unused-vars: 0, no-undef: 0 */
const adminAlbums = {
  template: '#admin-albums-template',
  components: {
    adminAlbum,
  },
  data() {
    return {
      albums: []
    }
  },
  created() {
    axios('/api/albums').then((result) => {
      this.albums = result.data
    })
  },
}
