/*  eslint no-undef: 0 no-new: 0 */

const router = new VueRouter({
  mode: 'history',
  routes: [
    { name: 'Admin', path: '/admin', component: admin },
    { name: 'Artists', path: '/artists', component: Artist },
    { path: '/artists/:name', component: Album },
    { name: 'Albums', path: '/albums', component: Album },
    { path: '/albums/:name', component: Song },
    { name: 'Songs', path: '/songs', component: Song },
    { name: 'Player', path: '/player', component: Player },
  ]
})

new Vue({
  router,
  components: {
    adminFolders,
    adminAlbums,

  }
}).$mount('#app')
