const db = require('../datasources')
const mm = require('music-metadata')
const { basename } = require('path')
const sharp = require('sharp')

const { Op } = db.Sequelize
const Artist = db.models.artist
const Album = db.models.album
const Song = db.models.song

const resizeCover = buf => sharp(buf).resize(200).toBuffer()

const addSongs = (paths) => {
  if (paths.length === 0) return
  const path = paths.shift()
  mm.parseFile(path, { duration: true }).then((metadata) => {
    const { common } = metadata
    Artist.findOrCreate({ where: { name: common.artist || 'Unknown' } })
      .then((artistResult) => {
        const artist = artistResult[0]
        const cover = common.picture && common.picture.length && common.picture[0] ?
          resizeCover(common.picture[0].data) : Promise.resolve(null)
        cover.then(resizedCover =>
          Album.findOrCreate({
            where: {
              artist_id: artist.id,
              title: common.album || 'Unknown',
            },
            defaults: {
              year: common.year,
              cover: resizedCover,
            },
          }).then((albumResult) => {
            const album = albumResult[0]
            Song.findOrCreate({
              where: { path },
              defaults: {
                title: common.title || basename(path),
                track: common.track.no || null,
                disk: common.disk.no || null,
                duration: metadata.format.duration || null,
                album_id: album.id
              }
            }).then((songResult) => {
              const song = songResult[0]
              console.log(`Added ${song.title}`)
              addSongs(paths)
            })
          }))
      })
  })
}

process.on('add songs', (paths) => {
  addSongs(paths)
})

process.on('remove songs on dir', (path) => {
  Song.destroy({ where: { path: { [Op.like]: `${path}%` } } })
    .then((result) => { console.log(`Removed ${result} songs from ${path}`) })
})
