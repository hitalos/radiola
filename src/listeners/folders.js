const debug = require('debug')('Radiola:listeners:folders')
const path = require('path')
const recursive = require('recursive-readdir')

const formats = [
  '.aac',
  '.flac',
  '.m4a',
  '.mp3',
  '.oga',
  '.ogg',
  '.wav',
  '.wma',
]

process.on('new folder', (dir) => {
  debug(`Search audio files in "${dir}"`)
  recursive(dir, (errPath, files) => {
    if (errPath) throw errPath
    if (files.length > 0) {
      const list = files.filter(filePath => formats.includes(path.extname(filePath)))
      process.emit('add songs', list)
    }
  })
})
