const player = require('play-sound')()

const playlist = {
  queue: [],
  proc: null,
  playing: false
}

process.on('add to playlist', (music) => {
  playlist.queue.push(music)
})

process.on('play list', () => {
  if (playlist.queue.length > 0 && !playlist.playing) {
    process.emit('play', playlist.queue[0])
  }
})

process.on('play', (music) => {
  playlist.playing = true
  playlist.proc = player.play(music.path, (err) => {
    if (err && !err.killed) throw err
  })
  playlist.proc.on('exit', () => {
    if (playlist.playing) {
      playlist.queue.splice(0, 1)
      process.emit('play next auto')
    }
  })
})

process.on('play next auto', () => {
  if (playlist.queue.length > 0) {
    process.emit('play', playlist.queue[0])
  } else {
    playlist.playing = false
  }
})

process.on('play next', () => {
  if (playlist.playing) {
    playlist.proc.kill()
  } else if (playlist.queue.length > 0) {
    process.emit('play', playlist.queue[0])
  } else {
    playlist.playing = false
  }
})

process.on('pause', () => {
  playlist.proc.kill('SIGSTOP')
})

process.on('resume', () => {
  playlist.proc.kill('SIGCONT')
})

process.on('stop', () => {
  playlist.playing = false
  playlist.proc.kill()
})

process.on('getList', (cb) => {
  cb(playlist.queue)
})
