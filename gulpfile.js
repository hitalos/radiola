const gulp = require('gulp')
const clean = require('gulp-clean')
const concat = require('gulp-concat')
const cssnano = require('gulp-cssnano')
const sass = require('gulp-sass')

gulp.task('scripts', () => {
  const min = process.env.NODE_ENV === 'production' ? '.min' : ''
  gulp.src([
    `./node_modules/axios/dist/axios${min}.js`,
    `./node_modules/vue/dist/vue${min}.js`,
    `./node_modules/vue-router/dist/vue-router${min}.js`,
    './src/resources/scripts/components/adminFolders.js',
    './src/resources/scripts/components/adminAlbum.js',
    './src/resources/scripts/components/adminAlbums.js',
    './src/resources/scripts/components/admin.js',
    './src/resources/scripts/components/artist.js',
    './src/resources/scripts/components/album.js',
    './src/resources/scripts/components/footer.js',
    './src/resources/scripts/components/header.js',
    './src/resources/scripts/components/song.js',
    './src/resources/scripts/components/player.js',
    './src/resources/scripts/app.js',
  ])
    .pipe(concat('app.js'))
    .pipe(gulp.dest('public/scripts'))
})

gulp.task('fonts', () => {
  gulp.src([
    './node_modules/font-awesome/fonts/*'
  ])
    .pipe(gulp.dest('public/styles/fonts'))
})

gulp.task('styles', ['fonts'], () => {
  gulp.src([
    'src/resources/styles/app.sass'
  ])
    .pipe(sass())
    .pipe(cssnano())
    .pipe(gulp.dest('public/styles'))
})

gulp.task('clean-js', () => gulp.src('public/scripts').pipe(clean()))
gulp.task('clean-css', () => gulp.src('public/styles').pipe(clean()))
gulp.task('clean', ['clean-js', 'clean-css'])

gulp.task('images', () =>
  gulp.src('src/resources/images/*')
    .pipe(gulp.dest('public/images')))

gulp.task('default', ['styles', 'scripts', 'images'])
